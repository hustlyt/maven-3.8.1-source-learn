package org.example;

import static org.junit.Assert.assertTrue;

import com.google.inject.*;
import com.google.inject.internal.SingletonScope;
import org.example.linkedbinding.HelloInterface;
import org.example.linkedbinding.impl.HelloInterfaceImpl;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testLinkedBinding()
    {
        AbstractModule module = new AbstractModule() {
            @Override
            public void configure() {
                final Binder binder = binder();
//                binder.bind(HelloInterface.class).to(HelloInterfaceImpl.class)
//                        .in(Singleton.class);
                binder.bind(HelloInterface.class).to(HelloInterfaceImpl.class)
                        .in(Scopes.SINGLETON);

            }
        };
        Injector injector = Guice.createInjector(module);
        HelloInterface instance = injector.getInstance(HelloInterface.class);
        HelloInterface instance1 = injector.getInstance(HelloInterface.class);

        System.out.println(instance);
        System.out.println(instance1);
    }


}
