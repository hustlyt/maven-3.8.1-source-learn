package org.example.bindingannotations;

import com.google.inject.Inject;

/**
 * 被注入的测试类
 */
public class InjectedClass {

    @Inject
    @Hello1
    private HelloInterface helloInterface;

    @Inject
    @Hello2
    private HelloInterface helloInterface2;

    public HelloInterface getHelloInterface() {
        return helloInterface;
    }

    public HelloInterface getHelloInterface2() {
        return helloInterface2;
    }
}
