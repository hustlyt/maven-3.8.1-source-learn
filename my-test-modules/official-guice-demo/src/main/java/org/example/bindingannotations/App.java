package org.example.bindingannotations;

import com.google.inject.*;
import com.google.inject.name.Names;

public class App {


    public static void main(String[] args) {
        {

            AbstractModule module = new AbstractModule() {
                @Override
                public void configure() {
                    final Binder binder = binder();
                    binder.bind(HelloInterface.class).annotatedWith(Hello1.class)
                            .to(HelloInterfaceImpl.class)
                            .in(Scopes.SINGLETON);
                    binder.bind(HelloInterface.class).annotatedWith(Hello2.class)
                            .to(HelloInterfaceImpl2.class)
                            .in(Scopes.SINGLETON);
                    binder.bind(InjectedClass.class);
                }
            };
            Injector injector = Guice.createInjector(module);

            /**
             * 1、依赖注入方式获取我们的依赖
             */
            InjectedClass injectedClass = injector.getInstance(InjectedClass.class);
            System.out.println(injectedClass.getHelloInterface());
            System.out.println(injectedClass.getHelloInterface2());

            /**
             * 2、直接从容器获取我们放进去的依赖
             */
            HelloInterface helloInterface1 = injector.getInstance(Key.get(HelloInterface.class, Hello1.class));
            HelloInterface helloInterface2 = injector.getInstance(Key.get(HelloInterface.class, Hello2.class));

            System.out.println(helloInterface1.getClass());
            System.out.println(helloInterface2.getClass());
        }

        System.out.println("-------------------------------");

        {
            AbstractModule module = new AbstractModule() {
                @Override
                public void configure() {
                    final Binder binder = binder();
                    binder.bind(HelloInterface.class).annotatedWith(Names.named("hello1"))
                            .to(HelloInterfaceImpl.class)
                            .in(Scopes.SINGLETON);
                    binder.bind(HelloInterface.class).annotatedWith(Names.named("hello2"))
                            .to(HelloInterfaceImpl2.class)
                            .in(Scopes.SINGLETON);
                    binder.bind(InjectedClassByNamedAnnotation.class);
                }
            };
            Injector injector = Guice.createInjector(module);

            /**
             * 1、依赖注入方式获取我们的依赖
             */
            InjectedClassByNamedAnnotation injectedClass = injector.getInstance(InjectedClassByNamedAnnotation.class);
            System.out.println(injectedClass.getHelloInterface());
            System.out.println(injectedClass.getHelloInterface2());

            /**
             * 2、直接从容器获取我们放进去的依赖
             */
            HelloInterface helloInterface1 = injector.getInstance(Key.get(HelloInterface.class, Names.named("hello1")));
            HelloInterface helloInterface2 = injector.getInstance(Key.get(HelloInterface.class, Names.named("hello2")));

            System.out.println(helloInterface1.getClass());
            System.out.println(helloInterface2.getClass());
        }
    }
}
