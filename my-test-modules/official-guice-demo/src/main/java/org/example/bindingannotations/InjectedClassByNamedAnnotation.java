package org.example.bindingannotations;

import com.google.inject.Inject;

import javax.inject.Named;

/**
 * 被注入的测试类
 */
public class InjectedClassByNamedAnnotation {

    @Inject
    @Named("hello1")
    private HelloInterface helloInterface;

    @Inject
    @Named("hello2")
    private HelloInterface helloInterface2;

    public HelloInterface getHelloInterface() {
        return helloInterface;
    }

    public HelloInterface getHelloInterface2() {
        return helloInterface2;
    }
}
