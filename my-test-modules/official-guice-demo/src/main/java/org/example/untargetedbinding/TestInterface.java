package org.example.untargetedbinding;

import com.google.inject.ProvidedBy;

@ProvidedBy(TestInterfaceProvider.class)
public interface TestInterface {
}
