package org.example.untargetedbinding;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.example.providerinterface.DatabaseProvider;

public class App {
    public static void main(String[] args) {
        {

            AbstractModule module = new AbstractModule() {
                @Override
                public void configure() {
                    final Binder binder = binder();
                    binder.bind(UtilService.class);
                }
            };
            Injector injector = Guice.createInjector(module);


            UtilService obj = injector.getInstance(UtilService.class);
            System.out.println(obj);
        }


        {

            AbstractModule module = new AbstractModule() {
                @Override
                public void configure() {
                    final Binder binder = binder();
                    binder.bind(TestInterface.class);
                }
            };
            Injector injector = Guice.createInjector(module);


            TestInterface obj = injector.getInstance(TestInterface.class);
            System.out.println(obj);
        }
    }
}
