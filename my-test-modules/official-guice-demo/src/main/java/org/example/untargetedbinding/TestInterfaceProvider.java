package org.example.untargetedbinding;

import com.google.inject.Provider;

public class TestInterfaceProvider implements Provider<TestInterface> {

    @Override
    public TestInterface get() {
        return new TestInterface() {
        };
    }
}
