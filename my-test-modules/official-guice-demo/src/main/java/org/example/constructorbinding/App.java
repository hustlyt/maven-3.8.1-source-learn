package org.example.constructorbinding;

import com.google.inject.AbstractModule;
import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Injector;

import java.io.File;

public class App {
    public static void main(String[] args) {
        AbstractModule module = new AbstractModule() {
            @Override
            public void configure() {
                final Binder binder = binder();
                try {
                    binder.bind(String.class).toInstance("org/example/constructorbinding/App.class");
                    binder.bind(File.class)
                            .toConstructor(File.class.getConstructor(String.class));
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        };
        Injector injector = Guice.createInjector(module);


        File obj = injector.getInstance(File.class);
        System.out.println(obj);

    }
}
