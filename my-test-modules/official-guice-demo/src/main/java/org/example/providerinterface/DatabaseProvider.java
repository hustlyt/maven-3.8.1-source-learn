package org.example.providerinterface;


import com.google.inject.Provider;

/**
 * 其实就是个工厂方法
 */
public class DatabaseProvider implements Provider<DatabaseProvider.DataBase> {

    @Override
    public DataBase get() {
        return new DataBase();
    }

    public static class DataBase {}
}
