package org.example.instancebinding;

import com.google.inject.*;
import com.google.inject.name.Names;
import org.example.bindingannotations.*;

public class App {
    public static void main(String[] args) {
        {

            AbstractModule module = new AbstractModule() {
                @Override
                public void configure() {
                    final Binder binder = binder();
                    binder.bind(TestInterface.class)
                            .toInstance(new TestInterfaceImpl());
                }
            };
            Injector injector = Guice.createInjector(module);

            /**
             * 1、依赖注入方式获取我们的依赖
             */
            TestInterface testInterface1 = injector.getInstance(TestInterface.class);
            TestInterface testInterface2 = injector.getInstance(TestInterface.class);
            System.out.println(testInterface1);
            System.out.println(testInterface2);

        }


        {
            AbstractModule module = new AbstractModule() {
                @Override
                public void configure() {
                    final Binder binder = binder();
                    binder.bind(String.class)
                            .annotatedWith(Names.named("dev"))
                            .toInstance("http://dev.test.com");
                    binder.bind(String.class)
                            .annotatedWith(Names.named("prod"))
                            .toInstance("http://prod.test.com");
                }
            };
            Injector injector = Guice.createInjector(module);


            String str1 = injector.getInstance(Key.get(String.class, Names.named("dev")));
            String str2 = injector.getInstance(Key.get(String.class, Names.named("prod")));
            System.out.println(str1);
            System.out.println(str2);
        }
    }

}
