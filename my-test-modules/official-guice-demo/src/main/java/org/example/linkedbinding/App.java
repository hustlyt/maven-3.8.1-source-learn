package org.example.linkedbinding;

import com.google.inject.*;
import org.example.linkedbinding.impl.HelloInterfaceImpl;

public class App {
    public static void main(String[] args) {

        AbstractModule module = new AbstractModule() {
            @Override
            public void configure() {
                final Binder binder = binder();
                binder.bind(HelloInterface.class).to(HelloInterfaceImpl.class)
                        .in(Scopes.SINGLETON);

            }
        };
        Injector injector = Guice.createInjector(module);
        HelloInterface instance = injector.getInstance(HelloInterface.class);
        HelloInterface instance1 = injector.getInstance(HelloInterface.class);

        System.out.println(instance);
        System.out.println(instance1);
    }

}
