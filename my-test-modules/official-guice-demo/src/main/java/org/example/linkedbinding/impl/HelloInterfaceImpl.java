package org.example.linkedbinding.impl;

import org.example.linkedbinding.HelloInterface;

public class HelloInterfaceImpl implements HelloInterface {
    @Override
    public void hello() {
        System.out.println("hello world");
    }
}
