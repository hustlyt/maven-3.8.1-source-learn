package org.example;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws MalformedURLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        URL urlForPluginApi = new URL("file:/C:\\Users\\Administrator\\.m2\\repository\\org\\example\\my-plugin-api\\1.0-SNAPSHOT\\my-plugin-api-1.0-SNAPSHOT.jar");
        URL urlForPluginImpl = new URL("file:/C:\\Users\\Administrator\\.m2\\repository\\org\\example\\my-plugin-implementation\\1.0-SNAPSHOT\\my-plugin-implementation-1.0-SNAPSHOT.jar");
        URL[] urls = {urlForPluginApi, urlForPluginImpl};
        URLClassLoader urlClassLoader = new URLClassLoader(urls,ClassLoader.getSystemClassLoader()){
//            @Override
//            public Class<?> loadClass(String name) throws ClassNotFoundException {
//                try{
//                    // 保证：寻找类时，优先查找自己的classpath，找不到，再去交给parent classloader
//                    Class<?> clazz = findClass(name);
//                    return clazz;
//                }catch (ClassNotFoundException exception ){
//                    return super.loadClass(name);
//                }
//            }
        };

        Class<?> implClazzByPluginClassloader = urlClassLoader.loadClass("org.example.MyMojoImplementation");
        MojoInterface mojoInterface = (MojoInterface) implClazzByPluginClassloader.newInstance();
        mojoInterface.execute();

        System.out.println( "Hello World!" );
    }
}
