package org.example;

import org.example.impl.HelloWorldInterfaceImpl;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        HelloWorldInterface helloWorldInterface = new HelloWorldInterfaceImpl();
        helloWorldInterface.hello();
    }
}
